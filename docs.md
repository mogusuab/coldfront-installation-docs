# 1.0 Coldfront Working Documentation

## 1.1 Prerequisites

## Installation Video
- **You can use the [installation video](https://mediaspace.msu.edu/media/HPCC+Coldfront+Local+Install/1_byridqcl) to help you install coldfront on your hpcc. This should be used alongside this docs**. 

### 1.1.1 Msu Gitlab Account

- Log in to your MSU gitlab account. If you do not have an account, go to https://gitlab.msu.edu/users/sign_in to create a gitlab account with your MSU email and password.

### 1.1.2 Git 
- Ensure you have git installed. You can run `git --version` to check if you have it installed. 
- If not, go to the [git official page](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) to install for your system.

<u>**HELP** Screenshots are attached to help you with your installation. </u>

### 1.1.3 Generate ssh Key
- In order to access the remote gitlab server from your hpcc account, you'll need to generate an `ssh key`. 

> To generate an SSH Key, follow the following instructions: 

1. Log in to your hpcc account using [ondemand](ondemand.hpcc.msu.edu)
2. Once You're on the home page, select the **Development Nodes** option and select the **dev-intl18 node** from the list.

![On demand ICER Homepage](image.png)

3. Once you click on the dev-intel18 node, this will open a shell terminal window. Type `pwd` to ensure you are on your home directory. This is where you will install the `ssh key`. It should show something like `/mnt/home/<yourAccountName>`.

- If youre not in the home directory, type `cd ~` This will take you to the home directory. 

![Terminal](image-1.png)
4. Once you're in the home directory, Create an SSH key using the instructions from the official [gitlab docs](https://docs.gitlab.com/ee/user/ssh.html#see-if-you-have-an-existing-ssh-key-pair).

5. Add the SSH key to your Gitlab account [like this](https://docs.gitlab.com/ee/user/ssh.html#add-an-ssh-key-to-your-gitlab-account)

> If you successfully finished these steps, you're all set up for the next part. 

# 2.0 Clone/Download Coldfront Installation Docs
- You should have set up your `gitlab account` and `ssh key` at this point. 

- Before downloading/cloning the repository from gitlab, ensure you have access to the repository. If not, inform your supervisor to get that sorted. 

## 2.1 Clone the Repository.

1. Go to the coldfront gitlab repository and either download the production repo as a tarball and unpack it in your home directory on an HPCC dev node, or git clone the current production repo.

- If you are using ssh to clone the repository, copy the ssh link. 

![Clone Repository](image-2.png)

- Write this on your terminal to clone to your home directory

```bash
    git clone git@gitlab.msu.edu:icer/coldfront.git
```

> If this runs succesfully, you should have a `coldfront` directory on your homepage. If not ensure you set up your `ssh` key properly and have access to the `coldfront directory`

## 2.2 Create a Virtual Environment.

- In the following section, you will run python commands from the terminal. In order to do that, you have to create a `python/conda virtual environment`

- To create a python virtual environment: Do the following on your Terminal. 

```bash
python3 -m venv <your environment name>
```
> Make sure to replace `<your environment name>` with your preferred environment name.

- For instance if i want to create a virtual  environment named `myenv,` this is what the code looks like: 

```bash
python3 -m venv myenv
```

- The activate the environment: `myenv`

```bash
source myenv/bin/activate
```
- You should see your environment now active:
-  Change directory into the `coldfront` directory:

```bash
cd coldfront
```

![Virtual environemnt](image-3.png)


## 2.3 Update `requirements.txt` in the coldfront Directory

- If it has not already been done so, in the `requirements.txt` file, change `psycopg2-binary==2.7.7` to `psycopg2-binary==2.8.6`.
- To open it on the terminal, you can use `vim` editor. 
- on the command line type: 

    ```bash
        vim requirements.txt
    ```
- This will open the file on the terminal. Type `i` to enter insert mode. This will allow you to edit the file. 

- Edit the specific line and once that's done, hit the `esc` button to enter command mode. 
- To exit and save, type `:wq` and then hit `enter`.

### 2.3.1 Install requirements. 

- with the virtual environment activated, run the following command on your terminal to install requirements: 

```bash
pip install -r requirements.txt
```

> If you encounter an error, ensure you're in the coldfront directory and your python virtual environment is activated. 

## 3.0 Setting Up Coldfront

### 3.1 Initialize Database - Run Initial Setup

- While in the coldfront directory,  run the Django specific command that will initialize the database:

    ```bash
    python manage.py initial_setup
    ```

> If you encounter any `python-ldap` errors, ensure that you are working from the `dev-intel18` development node.

### 3.2 Load the Test Data
- Load test data.

    ```bash
    python manage.py load_test_data
    ```

### 3.3 Check to see that the information is Loading. 

- Because this is running on the HPCC, you will need to use a web server running on the HPCC to access the server. Go to your local web browser, and open an ([Open OnDemand Session](https://docs.icer.msu.edu/Open_OnDemand/))

- Once the interactive desktop loads, click on `applications`, then `system tools` and launch the `terminal`

![ondemand](image-10.png)

- On the interactive desktop `terminal`, cd into the coldfront directory:

```bash
cd coldfront
```

- activate the virtual environment again

```bash
source ./myenv/bin/activate
```

- Next, run a local instance of the website.

```bash
python manage.py runserver 8008
```

- To view the landing page, click on the firefox icon at the top and into the searchbar type `localhost:8008/`

- You should land on a page like this: 
![test page](image-11.png)

- Kill the server by running `ctrl + c` on the terminal and `exit` to exit the terminal. 

## 4.0 Current HPCC Configuration
- Contact your supervisor about where the most up to date HPCC data is kept.

- You will want to copy this data to your `local_data` directory in your Coldfront installation.

> The data can also be found on the `/tmp` directory on `dev-intel18` under the name `coldfront_data.tar`

> When copying the files, untar the `coldfront_data.tar` file, then copy the specific files. Do not copy it as a folder.

- On your terminal on dev_intel18 on your `ondemand` workspace, type the following command to copy:

```bash
cp /tmp/coldfront_data.tar ./local_data/
```

> This will copy the `coldfront_data.tar` file to the `local_data` directory. This will however copy it as a folder. You need to extract the files. 

- On your terminal, navigate to your local_data directory

```bash
cd local_data
```

- Untar the files: 
```bash
tar -xvf coldfront_data.tar
```

- Remove the coldfront_data.tar folder
```bash
rm coldfront_data.tar
```

- To go back to the coldfront home directory: 
```bash
cd ..
```

## 5.0 Update User Permissions
- Locate the `import_projects_msuhpcc.py` file within your installation. You can do this by running the following command: 

    ```bash
    find -name "import_projects_msuhpcc.py"
    ```

- This command will show the file paths to the files. You need to update both files.

- Once you obtain the file paths, you can use `vim` in insert mode to make the changes like we did previously. For instance

```bash
vim ./coldfront/core/utils/management/commands/import_projects_msuhpcc.py
```

- If not already done, add the following two lines to the end of the file to change user permissions:

    ```bash
    os.chmod("/tmp/pi_users.csv", 0o777)
    os.chmod("/tmp/users_request_dates.csv", 0o777)
    ```

## 6.0 Reset Data and Load HPCC Data
- First, reset the data stored within the database in the following order:

    ```bash
    python manage.py reset_projects_msuhpcc
    python manage.py reset_users_msuhpcc
    python manage.py reset_resources_msuhpcc

> You might encounter an `Operational error: No such table`. This just means there is nothing to reset.


- Next, import the HPCC data in the following order:

    ```bash
    python manage.py import_resources_msuhpcc
    python manage.py import_users_msuhpcc 
    python manage.py import_is_pi_msuhpcc
    python manage.py import_projects_msuhpcc
    ```

> **Note**: Importing users may take upto 15mins. 

## 7.0 Reload the Web Server

- The new HPCC information should be loaded into the database now. Relaunch the web server. **(Like you did for the test data on the interactive desktop)**

    ```bash
    python manage.py runserver 8008
    ```

- Connect to the server by pasting the following into the Firefox url search bar:
    ```bash
    localhost:8008
    ```
- To login, use `admin` as the username and `test1234` as the password. The data should be viewable in the web browser.

